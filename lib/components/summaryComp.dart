import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:retoflutter/models/Country.dart';
import 'package:retoflutter/models/global.dart';
import 'package:retoflutter/providers/summaryProv.dart';

class SummaryComp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SummaryProv summaryProv = Provider.of<SummaryProv>(context);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: TabBarView(
          children: <Widget>[
            _list(summaryProv),
            _globalDetalle(summaryProv),
          ],
        ),
        bottomNavigationBar: TabBar(
          tabs: [
            Tab(
              icon: Icon(Icons.directions_car),
            ),
            Tab(
              icon: Icon(Icons.directions_transit),
            ),
          ],
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.grey,
          indicatorColor: Colors.greenAccent,
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorPadding: EdgeInsets.all(5.0),
        ),
      ),
    );
  }

  Widget _list(SummaryProv summaryProv) {
    return Column(
      children: <Widget>[
        RaisedButton(
          color: Colors.green,
          onPressed: () {
            summaryProv.changeOrder();
          },
          child: Icon(summaryProv.ascendent
              ? Icons.arrow_downward
              : Icons.arrow_upward),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: summaryProv.countries.length,
            itemBuilder: (context, position) {
              return ListTile(
                trailing: IconButton(
                  icon: Icon(Icons.details),
                  onPressed: () {
                    _showDetail(
                        context, summaryProv.countries.elementAt(position));
                  },
                ),
                title: Text(summaryProv.countries.elementAt(position).country),
                subtitle: Text(
                    "Casos Confirmados:: ${summaryProv.countries.elementAt(position).totalConfirmed}"),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _globalDetalle(SummaryProv summryProv) {
    Global _global = summryProv.global;
    return _global == null
        ? Text("no hay informacion disponible")
        : Column(
            children: <Widget>[
              Text(
                "Datos Totales A Nivel Mundial",
                style: TextStyle(fontSize: 25),
              ),
              Divider(
                thickness: 5.0,
              ),
              ListTile(
                title: Text("Nuevos Confirmados"),
                subtitle: Text("${_global.newConfirmed}"),
              ),
              ListTile(
                title: Text("Total Confirmados"),
                subtitle: Text("${_global.totalConfirmed}"),
              ),
              ListTile(
                title: Text("Nuvas muertes"),
                subtitle: Text("${_global.newDeaths}"),
              ),
              ListTile(
                title: Text("Total Muertes"),
                subtitle: Text("${_global.totalDeaths}"),
              ),
              ListTile(
                title: Text("Nuevos recuperados"),
                subtitle: Text("${_global.newRecovered}"),
              ),
              ListTile(
                title: Text("Total recuperados"),
                subtitle: Text("${_global.totalRecovered}"),
              )
            ],
          );
  }

  void _showDetail(BuildContext context, Country country) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(country.country),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                    "Fecha: ${country.date.year}-${country.date.month}-${country.date.day}"),
                ListTile(
                  title: Text("Nuevos Confirmados"),
                  subtitle: Text("${country.newConfirmed}"),
                ),
                ListTile(
                  title: Text("Total Confirmados"),
                  subtitle: Text("${country.totalConfirmed}"),
                ),
                ListTile(
                  title: Text("Nuvas muertes"),
                  subtitle: Text("${country.newDeaths}"),
                ),
                ListTile(
                  title: Text("Total Muertes"),
                  subtitle: Text("${country.totalDeaths}"),
                ),
                ListTile(
                  title: Text("Nuevos recuperados"),
                  subtitle: Text("${country.newRecovered}"),
                ),
                ListTile(
                  title: Text("Total recuperados"),
                  subtitle: Text("${country.totalRecovered}"),
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}
