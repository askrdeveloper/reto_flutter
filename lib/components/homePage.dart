import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:retoflutter/components/summaryComp.dart';
import 'package:retoflutter/providers/summaryProv.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Datos OMS"),
      ),
      body: ChangeNotifierProvider<SummaryProv>(
        create: (_) => SummaryProv(),
        child: SummaryComp(),
      ),
    );
  }
}
