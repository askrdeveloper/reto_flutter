import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:retoflutter/config/data.dart';
import 'package:retoflutter/config/routes.dart';
import 'package:retoflutter/models/Country.dart';
import 'package:retoflutter/models/global.dart';

class SummaryProv with ChangeNotifier {
  bool _isProcess = false;
  bool _ascendent = false;
  List<Country> _countries = [];
  Global _global;

  SummaryProv() {
    /*
    for (int i = 0; i < DataList.dataNow["Countries"].length; i++) {
      _countries.add(Country.ofJson(DataList.dataNow["Countries"][i]));
    }
    _countries.sort((a, b) {
      return a.totalConfirmed > b.totalConfirmed ? 1 : -1;
    });
    */

    getSummary();
  }

  getSummary() {
    isProcess = true;
    ascendent = false;
    http.get(Routes.sumary).then((response) {
      if (response.statusCode == 200) {
        Map<String, dynamic> data = jsonDecode(response.body);
        _global = Global.ofJson(data["Global"]);

        for (int i = 0; i < data["Countries"].length; i++) {
          _countries.add(Country.ofJson(data["Countries"][i]));
        }
        _countries.sort((a, b) {
          return a.totalConfirmed > b.totalConfirmed ? 1 : -1;
        });
      } else {
        print(response.body);
        throw new ErrorDescription("error al obtener datos");
      }
    }).timeout(Duration(seconds: 10), onTimeout: () {
      throw new ErrorDescription("tiempo de espera excedido");
    }).catchError((err) {
      print("sumErr>$err");
    }).whenComplete(() {
      isProcess = false;
    });
  }
  void changeOrder() {
    ascendent = !ascendent;
    countries = countries.reversed.toList();
    notifyListeners();
  }
  bool get isProcess => _isProcess;

  set isProcess(bool value) {
    _isProcess = value;
    notifyListeners();
  }

  bool get ascendent => _ascendent;

  set ascendent(bool value) {
    _ascendent = value;
  }

  List<Country> get countries => _countries;

  set countries(List<Country> value) {
    _countries = value;
  }


  Global get global => _global;

  set global(Global value) {
    _global = value;
  }


}
