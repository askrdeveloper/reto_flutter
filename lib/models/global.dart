class Global {
  int _newConfirmed;
  int _totalConfirmed;
  int _newDeaths;
  int _totalDeaths;
  int _newRecovered;
  int _totalRecovered;


  Global.ofJson(data){
    try {
      _newConfirmed = data["NewConfirmed"];
      _totalConfirmed = data["TotalConfirmed"];
      _newDeaths = data["NewDeaths"];
      _totalDeaths = data["TotalDeaths"];
      _newRecovered = data["NewRecovered"];
      _totalRecovered = data["TotalRecovered"];
    }catch(err){
      print("modGlob>${err}");
    }
  }

  int get totalRecovered => _totalRecovered;

  set totalRecovered(int value) {
    _totalRecovered = value;
  }

  int get newRecovered => _newRecovered;

  set newRecovered(int value) {
    _newRecovered = value;
  }

  int get totalDeaths => _totalDeaths;

  set totalDeaths(int value) {
    _totalDeaths = value;
  }

  int get newDeaths => _newDeaths;

  set newDeaths(int value) {
    _newDeaths = value;
  }

  int get totalConfirmed => _totalConfirmed;

  set totalConfirmed(int value) {
    _totalConfirmed = value;
  }

  int get newConfirmed => _newConfirmed;

  set newConfirmed(int value) {
    _newConfirmed = value;
  }

}