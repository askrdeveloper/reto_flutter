class Country{
  String _country;
  String _countryCode;
  String _slug;
  int _newConfirmed;
  int _totalConfirmed;
  int _newDeaths;
  int _totalDeaths;
  int _newRecovered;
  int _totalRecovered;
  DateTime _date;

  Country.ofJson(data){
    try {
      _country = data["Country"];
      _countryCode = data["CountryCode"];
      _slug = data["Slug"];
      _newConfirmed = data["NewConfirmed"];
      _totalConfirmed = data["TotalConfirmed"];
      _newDeaths = data["NewDeaths"];
      _totalDeaths = data["TotalDeaths"];
      _newRecovered = data["NewRecovered"];
      _totalRecovered = data["TotalRecovered"];
      _date = DateTime.parse(data["Date"]);
    }catch(err){
      print("modCountryErr$err");
    }
  }

  DateTime get date => _date;

  set date(DateTime value) {
    _date = value;
  }

  int get totalRecovered => _totalRecovered;

  set totalRecovered(int value) {
    _totalRecovered = value;
  }

  int get newRecovered => _newRecovered;

  set newRecovered(int value) {
    _newRecovered = value;
  }

  int get totalDeaths => _totalDeaths;

  set totalDeaths(int value) {
    _totalDeaths = value;
  }

  int get newDeaths => _newDeaths;

  set newDeaths(int value) {
    _newDeaths = value;
  }

  int get totalConfirmed => _totalConfirmed;

  set totalConfirmed(int value) {
    _totalConfirmed = value;
  }

  int get newConfirmed => _newConfirmed;

  set newConfirmed(int value) {
    _newConfirmed = value;
  }

  String get slug => _slug;

  set slug(String value) {
    _slug = value;
  }

  String get countryCode => _countryCode;

  set countryCode(String value) {
    _countryCode = value;
  }

  String get country => _country;

  set country(String value) {
    _country = value;
  }
}